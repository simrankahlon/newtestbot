import json

def fetch_response(intent_name):
    filepath = "./message_bank/"+intent_name+".json"
    with open(filepath,'r') as f:
        message = json.load(f)
    return message